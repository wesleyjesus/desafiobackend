# Desafio de Backend
----

Imagine o cenário em que você trabalha em uma equipe responsável por administrar os produtos que serão vendidos dentro do e-commerce e surgiu a necessidade de criar uma nova solução para tratar o mesmo. Porém, por se tratar de um projeto que terá anos de vida, deverá ser construído dentro das melhores práticas do mercado, TDD, Solid, DDD e etc.

*Praticas do Clean Code será um diferencial*

## Requisitos

O  desafio deve conter dois projetos

* Client Side  
* Server Side

Para o desenvolvimento do client-side utilize a tecnologia que que melhor lhe convém, porem atendendo os seguintes requisitos.

* Cadastro
* Busca
* Alteração
* Exclusão

Para o server-side é necessário utilizar a stack informada. 

* ASP.NET CORE > 2.0 ou Web API (NET Standard)

*Atenção*

O retorno do serviço deve ser próximo ao link abaixo

[Mock retorno json](http://www.mocky.io/v2/5d0a9d642f00002800e3eca2)